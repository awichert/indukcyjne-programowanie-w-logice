\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mgr}[2016/04/13 Szablon pracy magisterskiej UAM]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[polish]{babel}
\selectlanguage{polish}
\usepackage{graphicx}

% even uglier trick for oneside printing/binding margins
\if@twoside%
   {}
\else%
   \setlength{\oddsidemargin}{89pt}
   \setlength{\evensidemargin}{\oddsidemargin}
\fi%  


\newcommand{\institutionLogo}[1][logo-UAM]{\def\@institutionLogo{\includegraphics[height=3cm]{{#1}}}}
\newcommand{\institution}[1][Uniwersytet im. Adama Mickiewicza w Poznaniu]{\def\@institution{#1}}
\newcommand{\faculty}[1][Wydzia\l{} Matematyki i Informatyki]{\def\@faculty{#1}}
\newcommand{\fieldOfStudy}[1]{\def\@fieldOfStudy{#1}}
\newcommand{\specialty}[1]{\def\@specialty{#1}}

\newcommand{\subtitle}[1]{\def\@subtitle{#1}}
\newcommand{\supervisor}[1]{\def\@supervisor{#1}}
\newcommand{\studentNo}[1]{\def\@studentNo{#1}}

\setlength{\emergencystretch}{3em}

\renewcommand{\maketitle}{%
	\newpage\thispagestyle{empty}
	\begin{center}%
		\let \footnote \thanks

		% define institution & faculty
		\ifdefined\@institutionLogo {} \else \institutionLogo{} \fi
		\ifdefined\@institution {} \else \institution{} \fi
		\ifdefined\@faculty {} \else \faculty{} \fi

		% show institution name, faculty & logo
		{\Large \@institution \par}%
		{\large \@faculty \par}%
		\@institutionLogo
	\end{center}%

	\vskip 5em%

	{\large
		\lineskip .5em%
		\begin{tabular}[t]{c}%
			\@author
		\end{tabular}\par
		\begin{tabular}[t]{c}%
			\small{Nr albumu}: \textbf{\@studentNo}
		\end{tabular}\par
	}%

	\vskip 2.5em%

	\begin{center}%
		{\LARGE \@title \par}
		{\Large \@subtitle}
	\end{center}%

	\vskip 1.5em%

	\ifdefined\@fieldOfStudy
		\vskip 1em%
		{\small{Praca magisterska na kierunku}: \par} 
		{\textbf{\@fieldOfStudy} \par}
	\fi
	\ifdefined\@specialty
	\vskip 1em%
	{\small{Specjalność}: \par} 
	{\textbf{\@specialty} \par}
	\fi
	\ifdefined\@supervisor
		\vskip 1em%
		{\small{Promotor}: \par}
		{\textbf{\@supervisor} \par}
	\fi

	\vfill

	\begin{center}
		{\large \@date}%
	\end{center}
	\vskip 1.5em
	\cleardoublepage
}


\newcommand{\makedeclaration}{
	\newpage
	\begin{flushright}
		\makebox[0.3\textwidth]{Poznań, dnia \dotfill}\\
		\makebox[0.3\textwidth]{\centering\tiny (data)}
	\end{flushright}
	\vskip 5em
	\section*{\centering Oświadczenie}
	\par
	Ja, niżej podpisana \textbf{\@author}, studentka Wydziału Matematyki i Informatyki Uniwersytetu im. Adama Mickiewicza w Poznaniu, oświadczam, że przedkładaną pracę dyplomową pt. \textbf{\textit{"{\@title}"}} napisałam samodzielnie. Oznacza to, że przy pisaniu pracy, poza niezbędnymi konsultacjami, nie korzystałam z pomocy innych osób, a w szczególności nie zlecałam opracowania rozprawy lub jej części innym osobom, ani nie odpisywałam tej rozprawy lub jej części od innych osób.
	\par
	Oświadczam również, że egzemplarz pracy dyplomowej w wersji drukowanej jest całkowicie zgodny z egzemplarzem pracy dyplomowej w wersji elektronicznej.
	\par
	Jednocześnie przyjmuję do wiadomości, że gdyby powyższe oświadczenie okazało się nieprawdziwe, decyzja o wydaniu mi dyplomu zostanie cofnięta.
	\par
	Jednocześnie przyjmuję do wiadomości, że przypisanie sobie, w pracy dyplomowej, autorstwa istotnego fragmentu lub innych elementów cudzego utworu lub ustalenia naukowego stanowi podstawę stwierdzenia nieważności postępowania w sprawie nadania tytułu zawodowego.
	\par
	\vskip 1em
	[\textbf{TAK}]* - wyrażam zgodę na udostępnianie mojej pracy w czytelni Archiwum UAM
	\par
	[\textbf{TAK}]* - wyrażam zgodę na udostępnianie mojej pracy w zakresie koniecznym do ochronymojego prawa do autorstwa lub praw osób trzecich
	\par
	\vskip 2em
	\begin{small}
	* Należy wpisać TAK w przypadku wyrażenia zgody na udostępnianie pracy w czytelni Archiwum UAM, NIE w przypadku braku zgody. Niewypełnienie pola oznacza brak zgody na udostępnianie pracy.
	\end{small}
	\vfill
	\begin{flushright}
		\makebox[0.5\textwidth]{\dotfill}\\
		\makebox[0.5\textwidth]{\centering\tiny (czytelny podpis studenta)}
	\end{flushright}
	\cleardoublepage
}
