# Inductive logic programming

## Experiment

### Local
[Run instructions & description](./experiment/README.md)

### Public
Visualization aviable at [s340951.projektstudencki.pl](http://s340951.projektstudencki.pl).

## Master's Thesis
This paper describes the problem of inductive logic programming (ILP) and provides the basic knowledge needed to understand it. The motivation of the author is to extend the experiment presented in the article "Inductive Logic Programming with Large-Scale Unstructured Data". An interesting aspect is to find the optimal gameplay for the chess endgame with white King and white Rook against the black King (KRK). GOLEM tool developed by Professor Stephen H. Muggleton was used to deduce the rules. Data provided with the model experiment was also used in the author's solution. This paper explains process of reproducing model experiment and expanding it step-by-step.

[Compiled thesis](./masters-thesis/praca_magisterska.pdf)

### Usefull LaTeX packages documentation

* [chessboard](http://mirrors.concertpass.com/tex-archive/macros/latex/contrib/chessboard/chessboard.pdf)
* [forest](http://piotrkosoft.net/pub/mirrors/CTAN/graphics/pgf/contrib/forest/forest-doc.pdf)
* [listings](http://piotrkosoft.net/pub/mirrors/CTAN/macros/latex/contrib/listings/listings.pdf)
* [imakeidx](https://mirror.hmc.edu/ctan/macros/latex/contrib/imakeidx/imakeidx.pdf)