# Experiments
## Model
### Description
Description for given model experiment aviable [here](./chess-model.md).
### Bibliography
[M. Bain, A. Srinivasan: Inductive Logic Programming With Large-Scale Unstructured Data](./../masters-thesis/bibliography/bain-srinivasan-ilp-with-large-scale-unstructured-data.pdf)

## Carried
### Description
Description for carried experiment aviable [here](./chess-carried.md).


# Used tools
Golem tool description given [here](./golem/src/README). 
