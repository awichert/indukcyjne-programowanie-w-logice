#!/usr/bin/swipl

:- include('krk.b').
:- include('krk.r').

moveKing(C, R, NC, NR) :- diff(C, NC, d1), diff(R, NR, d1).
moveKing(C, R, NC, NR) :- diff(C, NC, d0), diff(R, NR, d1).
moveKing(C, R, NC, NR) :- diff(C, NC, d1), diff(R, NR, d0).
moveRook(C, R, NC, NR) :- diff(C, NC, d0), diff(R, NR, D), not(D = d0).
moveRook(C, R, NC, NR) :- diff(C, NC, D), not(D = d0), diff(R, NR, d0).

find(WKc, WKr, WRc, WRr, BKc, BKr, [[0, WKc, WKr, WRc, WRr, BKc, BKr]], 0) :- krk(0, WKc, WKr, WRc, WRr, BKc, BKr).
find(WKc, WKr, WRc, WRr, BKc, BKr, [[Moves,  WKc, WKr, WRc, WRr, BKc, BKr] | Rest], Moves) :- 
	krk(Moves, WKc, WKr, WRc, WRr, BKc, BKr),
	moveKing(WKc, WKr, WKc2, WKr2),
	moveKing(BKc, BKr, BKc2, BKr2),
	MovesX is Moves - 1,
	find(WKc2, WKr2, WRc, WRr, BKc2, BKr2, Rest, MovesX).
find(WKc, WKr, WRc, WRr, BKc, BKr, [[Moves, WKc, WKr, WRc, WRr, BKc, BKr] | Rest], Moves) :- 
	krk(Moves, WKc, WKr, WRc, WRr, BKc, BKr),
	moveRook(WRc, WRr, WRc2, WRr2),
	moveKing(BKc, BKr, BKc2, BKr2),
	MovesX is Moves - 1,
	find(WKc, WKr, WRc2, WRr2, BKc2, BKr2, Rest, MovesX).