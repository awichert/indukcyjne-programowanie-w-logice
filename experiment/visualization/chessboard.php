<?php

$find = ['c','2','c','5','a','1'];
if(isset($_GET['find']) && $_GET['find']) {
	$f = str_split(strtolower($_GET['find']));
	if (count($f) == 6)
		$find = $f;
}

$cmd = sprintf("swipl -q -f ./../chess-carried/find.pl -g 'findall([P], find(%s,P,M), Paths),print(Paths),halt'", implode(',', $find));

exec($cmd, $output);

$count = [];

if ($output[0] == '[]')
	$output = null;
else {
	$output = explode(']],[[', $output[0]);
	foreach ($output as $i => &$o) {
		$o = trim($o, '[]');
		$o = explode('],[', $o);
		$count[count($o)] += 1;
	}
}

function renderChessboard($krk = null) {
	$board = '';
	for ($i = 8; $i > 0; $i--) {
		$board .= '<tr>';
		$board .= '<th>'. $i .'</th>';
		for ($j = 'a'; $j < 'i'; $j++) {
			$board .= '<td id="chessboard-'. $j . $i .'">';
			if ($krk) {
				if ($krk[1] == $j && $krk[2] == $i)
					$board .= '&#9812;';	// White King
				else if ($krk[3] == $j && $krk[4] == $i)
					$board .= '&#9814;';	// White Rook
				else if ($krk[5] == $j && $krk[6] == $i)
					$board .= '&#9818;';	// Black King
				else $board .= '&nbsp;';
			} else $board .= '&nbsp;';
			$board .= '</td>';
		}
		$board .= '<th>'. $i .'</th>';
		$board .= '</tr>';
	}
	return $board;
}
?>

<html>
  <head>
    <title>ILP - Experiment Visualization - Chessboard</title>
    <meta content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
    
    @font-face {
		font-family: 'Arial Unicode MS';
		src: url('arialunicodems.ttf')  format('truetype');
	}

	ul.finds.collapse {
		display: none;
	}
	
	ul.finds ol.steps {
		
	}
    
    table.chess-pieces td,
    table.chessboard td {
		font-size: 32px;
		font-family: 'Arial Unicode MS', 'Arial', sans-serif;
    }
    
    table.chess-pieces {
		text-align: center;
		vertical-align: middle;
		background-color: white;
    }
    
    table.chessboard {
		border-collapse: collapse;
		text-align: center;
		vertical-align: middle;
		width: 80vmin;
		max-width:	80vmin;
		height: 80vmin;
		min-height: 80vmin;
		table-layout: fixed;
		position: relative;
		background-color: transparent;
    }
    
    table.chessboard caption {
		height: 10vmin;
    }
    
    table.chessboard tbody.chessboard-endgame:not(.current),
    table.chessboard tbody.hidden {
		display: none;
    }
    
    table.chessboard th,
    table.chessboard td {
		width: 10%;
		min-width: 10%;
		max-width: 10%;
		height: 10%;
		min-height: 10%;
		max-height: 10%;
		font-size: 4vmin;
		background-color: transparent;
    }
    
    table.chessboard td {
		font-size: 7vmin;
		background-color: rgba(255,255,255,0.5);
    }
    
    table.chessboard tr:nth-child(odd) td:nth-child(odd),
    table.chessboard tr:nth-child(even) td:nth-child(even) {
		background-color: rgba(0,0,0,0.5);
    }
    
    table.chessboard tbody tr:first-child > td {
		border-top: 1px solid black;
    }
    
    table.chessboard tbody tr:last-child > td {
		border-bottom: 1px solid black;
    }
    
    table.chessboard tbody tr > td:first-of-type {
		border-left: 1px solid black;
    }
    
    table.chessboard tbody tr > td:last-of-type {
		border-right: 1px solid black;
    }
    
    </style>
    <script type="text/javascript">
		function highlightCol(col) {
		console.log();
			document.getElementById(col).classList.add('highlight');
		}
		function rmHighlightCol(col) {
			document.getElementById(col).classList.remove('highlight');
		}
		
		window.onload = function() {
			var rank = 'abcdefgh'.split('');
			var elem = null;
			for (var f = 0; f < 8; f += 1)
				for (var r = 1; r < 9;  r += 1) {
					elem = document.getElementById('chessboard-' + rank[f] + r);
				}
		}
		function hideAllEndgame() {
			[].forEach.call(document.getElementsByClassName('chessboard-endgame current'), function(elem) {
				elem.classList.remove('current');
			});
		}
		function showEndgame(e, s) {
			document.getElementById('endgame-blank').classList.add('hidden');
			hideAllEndgame();
			document.getElementById('endgame-' + e + '-' + s).classList.add('current');
		}
		function blankEndgame() {
			document.getElementById('endgame-blank').classList.remove('hidden');
			hideAllEndgame();
		}
		function nextEndgame() {
			var elem = document.getElementsByClassName('chessboard-endgame current')[0];
			if (elem && elem.nextElementSibling && elem.nextElementSibling.id.substr(0, 10) == elem.id.substr(0, 10)) {
				hideAllEndgame();
				elem.nextElementSibling.classList.add('current');
			}
		}
		function prevEndgame() {
			var elem = document.getElementsByClassName('chessboard-endgame current')[0];
			if (elem && elem.previousElementSibling && elem.previousElementSibling.id.substr(0, 10) == elem.id.substr(0, 10)) {
				hideAllEndgame();
				elem.previousElementSibling.classList.add('current');
			}
		}
    </script>
  </head>
  <body>
	<h1>Experiment Visualization</h1>
	<h2>Chess Pieces</h2>
	<table class="chess-pieces">
		<caption>Chess Figures</caption>
		<tbody>
			<tr>
				<th>White</th><td>&#9812;</td><td>&#9813;</td><td>&#9814;</td><td>&#9815;</td><td>&#9816;</td><td>&#9817;</td>
			</tr>
			<tr>
				<th>Figure</th><th>King</th><th>Queen</th><th>Rook</th><th>Bishop</th><th>Knight</th><th>Pawn</th>
			</tr>
			<tr>
				<th>Black</th><td>&#9818;</td><td>&#9819;</td><td>&#9820;</td><td>&#9821;</td><td>&#9822;</td><td>&#9823;</td>
			</tr>
		</tbody>
	</table>
	<h2>Finding endgame path</h2>
	<h3>Query</h3>
	<p>Enter KRK endgame pieces positions:</p>
	<form>
	<input name="find" type="text" value="<?= $_GET['find'] ?>"/>
	<input type="submit" value="Find"/>
	</form>
	<h3>Query results</h3>
	<p>All results found: <strong><?= count($output) ?></strong></p>
	<?php if(!empty($count)): ?>
	<p>Results per <i>depth</i>:
		<ul>
			<?php foreach($count as $d => $c): ?>
				<li>Depth&nbsp;=&nbsp;<?= $d ?>: <strong><?= $c ?></strong></li>
			<?php endforeach; ?>
		</ul>
	</p>
	<?php endif; ?>
	<?php if($output): ?>
	
	<ul class="finds">
	<?php foreach($output as $e => $endgame): ?>
		<?php if($e == 10): ?>
		</ul>
		<a onclick="this.nextElementSibling.classList.toggle('collapse');">Show/hide more</a>
		<ul class="finds collapse">
		<?php endif;?>
		<li>
			<a href="#chessboard" onclick="showEndgame(<?= $e . ', ' . (count($endgame)-1) ?>);">Show on chessboard</a>
			<ol class="steps">
			<?php
				foreach($endgame as $s => $step):
				$step = explode(',', $step);
			?>
				<li>WTW:&nbsp;<?= $step[0] ?>, &#9812;<?= $step[1].$step[2] ?> &#9814;<?= $step[3].$step[4] ?> &#9818;<?= $step[5].$step[6] ?></li>
			<?php endforeach; ?>
		
			</ol>
		</li>
	<?php endforeach; ?>
	</ul>
	<?php endif; ?>
	<h3>Chessboard</h3>
	<table class="chessboard" id="chessboard">
		<caption>
			<button onclick="blankEndgame();">Blank</button>
			<button onclick="prevEndgame();">Prev step</button>
			<button onclick="nextEndgame();">Next step</button>
		</caption>
		<colgroup span="10">
			<col/><col id="chessboard-col-1"/><col id="chessboard-col-2"/><col id="chessboard-col-3"/><col id="chessboard-col-4"/><col id="chessboard-col-5"/><col id="chessboard-col-6"/><col id="chessboard-col-7"/><col id="chessboard-col-8"/><col/>
		</colgroup>
		<thead>
			<tr>
				<th></th>	<th>a</th>	<th>b</th>	<th>c</th>	<th>d</th>	<th>e</th>	<th>f</th>	<th>g</th>	<th>h</th>	<th></th>
			</tr>
		</thead>
		<tbody id="endgame-blank" >
			<?= renderChessboard(); ?>
		</tbody>
		<?php
			if ($output):
			foreach($output as $e => $endgame):
				foreach($endgame as $s => $step):
				$step = explode(',', $step);
		?>
		<tbody id="endgame-<?= $e . '-' . $step[0] ?>" class="chessboard-endgame">
			<?= renderChessboard($step); ?>
		</tbody>
		<?php endforeach; endforeach; endif; ?>
		<tfoot>
			<tr>
				<th></th>	<th>a</th>	<th>b</th>	<th>c</th>	<th>d</th>	<th>e</th>	<th>f</th>	<th>g</th>	<th>h</th>	<th></th>
			</tr>
		</tfoot>
	</table>
  </body>
</html>
