# Model experiment 
Experiment explained in section 3. of master thesis.

Subsection 3.1. introduces the problem.

Subsection 3.2. gives details about data delivered with model experiment.

Subsection 3.3. describes how experiment was handled in paper [Inductive Logic Programming With Large-Scale Unstructured Data](./../masters-thesis/bibliography/bain-srinivasan-ilp-with-large-scale-unstructured-data.pdf).