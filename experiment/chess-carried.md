# Carried experiment 
Experiment is described in section 4. of master thesis.

Subsection 4.1. explains step-by-step how model experiment was handled.

Subsection 4.2. introduces expansion of model experiment.

Subsection 4.3. describes visualization method.

## Generating data
Run:
```bash
./experiment/chess-carried/generate-n.sh N
```

Where `N` is desired depth of win and is _between 0 and 16_.

## Running visualization application
Run:
```bash
php -S localhost:8080 ./experiment/visualization/chessboard.php
```