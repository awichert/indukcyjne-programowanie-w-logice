#!/bin/bash

DEPTH=$1
PATTERN='% BTM depth '
DIR='./krk-experiment'
GOLEM='./../../golem/src/golem'
LASTLINE=1
mkdir -p "$DIR"
"" > ./krk.r
cp ./krk.b "$DIR/krk0.b"
sed -i 's/krk\/7/krk\/6/g' "$DIR/krk0.b"

for (( d=0; d<=DEPTH; )); do
	if [ $d -gt 0 ]; then
		cp "$DIR/krk0.b" "$DIR/krk$d.b"
	fi
	
	FILENAME="$DIR/krk$d"
	((d+=1))
	p=$PATTERN
	if [ $d -lt 10 ]; then
		p+='0'
	fi
	p+=$d
	
	LINE=$(grep -n -m 1 "$p" krk_win | cut -d: -f1)
	tail -n +$LINE krk_win | shuf -n 100 | sed -e 's/krk(-\?[0-9]\+,/krk(/g' > "$FILENAME.n"
	head -n $LINE krk_win | tail -n +$LASTLINE | sed -e 's/krk(-\?[0-9]\+,/krk(/g' > "$FILENAME.f"
	$GOLEM $FILENAME
	LASTLINE=$LINE
	
	cat "$FILENAME.r" | sed -e "s/krk(/krk($((d-1)),/g" >> ./krk.r
done